# Example 2 - QGIS Processing Tool - Export Map

Second example presented at the TUFLOW user conference 2021 in the Python workshop. This project contains a Python script that can be added to the processing toolbox in QGIS to batch export maps.

This tool produces a depth map with water level contours from the required inputs:
  1. Depth result raster
  2. Water level result raster which will be automatically processed into contours
  3. Pre-defined depth raster and contour vector styling as .QML files
  4. Print Layout template (.QPT)

The current tool should be used as a starting point for personalised / tailored map generation or as a springboard for more advanced tools such as including other result types in the same process (e.g. velocity maps), including labels, annotations, and dynamic text.

## Dependencies
The script is intended to be added to the QGIS processing toolbox and all dependencies should be installed with the QGIS Python environment.

The script was tested in QGIS 3.20, but may work on earlier versions of QGIS.

## Installation
1. Clone this repository<br>

```
git clone https://gitlab.com/tuflow-user-group/tuflow_user_conference_2021/example2_qgis_processing_toolbox.git
```

3. Open QGIS
4. Add the script `export_map.py` as a tool to the Processing Toolbox
   1. Open the Processing Toolbox Panel<br><b>View >> Panels >> Processing Toolbox</b>
   2. At the top of the Processing Toolbox select<br><b>Scripts >> Add Script To Toolbox..</b><br>
   ![add_script_to_toolbox](/uploads/827d5589bfc5643d49e98d59e0b1afd4/add_script_to_toolbox.png)

## Usage
Follow the steps below to run the tool:

1. Open QGIS
2. Load all base layers (non-result layers) that will be included on the map (it's ok to add more than required - these will need to be selected in the tool)
3. Open a sample result depth raster - a sample depth result is required in the workspace for the map legend to paint correctly
   1. apply desired symbology
   2. export style for layer as a .QML
   3. rename the layer in the Layers Panel (this will be the name used in the map legend)<br>
   e.g.<Br>
   ![legend_depth_example](/uploads/08b1f17fcb3c5a50b0efdeae01b3bb81/legend_depth_example.PNG)
4. Open a sample contour result (this will require you to manually contour one water level raster manually) - a sample contour result is required in the workspace for the map legend to paint correctly
   1. apply desired symbology
   2. export style for layer as a .QML
   3. rename the layer in the Layers Panel (this will be the name in the map legend)<br>
   e.g. (please ignore the poor interval definitions!)<Br>
   ![legend_contour_example](/uploads/a16f0cce0d74d35a4cfb4eab8ee25ccb/legend_contour_example.PNG)
5. Using your base layers and sample results, setup a print layout and export the final product as a template (.QPT)
6. Open the 'Export Map' tool in the processing toolbox
7. Inputs:
   1. Depth Grid - result depth raster - not required to be currently open in workspace and does not need to match the sample result from step 3
   2. Water Level Grid - result water level raster - not required to be currently open in workspace
   3. Base Layers - choose which currently open layers to include on the map (don't include any result layers)
   4. crs - select the coordinate reference system of your water level raster
   5. Depth Style - the depth raster styling .QML
   6. Contour Style - the contour styling .QML
   7. Print Layout - the print layout (.QPT) that will be used for the map
   8. Output PDF - location of output map
   9. Water Level Contour Inverval - the contour interval that the water level raster will be processed into (units will be the same as the raster units)
   10. Select 'Run'

The tool can also be run as a batch process which can be initialised by selecting 'Run as Batch Process' in the tool dialog or by right clicking the tool in the Processing Toolbox and selecting 'Run as Batch Process'

### Example output:

![export_map_dialog_populated](/uploads/6ef6f13627f6de92e7f2e33821cd4891/export_map_dialog_populated.PNG)

![export_map_dialog_finished](/uploads/ec220dae7263a0b23ee86a2d2680f02c/export_map_dialog_finished.PNG)

![export_map_finished_map](/uploads/9008febff0b495ef03f331cf095f8469/export_map_finished_map.PNG)

## Support
For any issues, please contact [support@tuflow.com](mailto:support@tuflow.com).

## License
This project is licensed under GNU GENERAL PUBLIC LICENSE Version 3.
