"""
Model exported as python.
Name : Export Map
Group :
With QGIS : 32002
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterFile
from qgis.core import QgsProcessingParameterRasterLayer
from qgis.core import QgsProcessingParameterMultipleLayers
from qgis.core import QgsProcessingParameterCrs
from qgis.core import QgsProcessingParameterNumber
from qgis.core import QgsProcessingParameterDefinition
import processing
from qgis.core import QgsPrintLayout, QgsReadWriteContext, QgsCoordinateReferenceSystem
from PyQt5.QtXml import QDomDocument


class ExportMap(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterRasterLayer('DepthGrid', 'Depth Grid', defaultValue=None))
        self.addParameter(QgsProcessingParameterRasterLayer('WaterLevelGrid', 'Water Level Grid', defaultValue=None))
        param = QgsProcessingParameterNumber('ContourInterval', 'Water Level Contour Interval',
                                             type=QgsProcessingParameterNumber.Double, minValue=0,
                                             maxValue=1.79769e+308, defaultValue=0.5)
        param.setFlags(param.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(param)
        self.addParameter(
            QgsProcessingParameterMultipleLayers('BaseLayers', 'Base Layers', layerType=QgsProcessing.TypeMapLayer,
                                                 defaultValue=None))
        self.addParameter(QgsProcessingParameterCrs('crs', 'crs', defaultValue='EPSG:4326'))
        self.addParameter(
            QgsProcessingParameterFile('DepthStyle', 'Depth Style', behavior=QgsProcessingParameterFile.File,
                                       fileFilter='QML (*.qml *.QML)', defaultValue=None))
        self.addParameter(
            QgsProcessingParameterFile('ContourStyle', 'Contour Style', behavior=QgsProcessingParameterFile.File,
                                       fileFilter='QML (*.qml *.QML)', defaultValue=None))
        self.addParameter(
            QgsProcessingParameterFile('PrintLayout', 'Print Layout', behavior=QgsProcessingParameterFile.File,
                                       fileFilter='QPT (*.qpt *.QPT)', defaultValue=None))
        self.addParameter(
            QgsProcessingParameterFile('OutputPDF', 'Output PDF', behavior=QgsProcessingParameterFile.File,
                                       fileFilter='PDF (*.pdf *.PDF)', defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(7, model_feedback)
        results = {}
        outputs = {}

        # Set layer style
        alg_params = {
            'INPUT': parameters['DepthGrid'],
            'STYLE': parameters['DepthStyle']
        }
        outputs['SetDepthLayerStyle'] = processing.run('native:setlayerstyle', alg_params, context=context,
                                                       feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        feedback.setCurrentStep(2)
        if feedback.isCanceled():
            return {}

        # Contour
        alg_params = {
            'BAND': 1,
            'CREATE_3D': False,
            'EXTRA': '',
            'FIELD_NAME': 'ELEV',
            'IGNORE_NODATA': False,
            'INPUT': parameters['WaterLevelGrid'],
            'INTERVAL': parameters['ContourInterval'],
            'NODATA': None,
            'OFFSET': 0,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['Contour'] = processing.run('gdal:contour', alg_params, context=context, feedback=feedback,
                                            is_child_algorithm=True)

        feedback.setCurrentStep(3)
        if feedback.isCanceled():
            return {}

        # Assign projection
        alg_params = {
            'CRS': QgsCoordinateReferenceSystem(parameters['crs'].authid()),
            'INPUT': outputs['Contour']['OUTPUT'],
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['AssignContourProjection'] = processing.run('native:assignprojection', alg_params, context=context,
                                                            feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(4)
        if feedback.isCanceled():
            return {}

        # Set layer style
        alg_params = {
            'INPUT': outputs['AssignContourProjection']['OUTPUT'],
            'STYLE': parameters['ContourStyle']
        }
        outputs['SetContourLayerStyle'] = processing.run('native:setlayerstyle', alg_params, context=context,
                                                         feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(5)
        if feedback.isCanceled():
            return {}

        feedback.setCurrentStep(6)
        if feedback.isCanceled():
            return {}

        # layers for map
        layers = [
            outputs['SetContourLayerStyle']['OUTPUT'],  # contour layer
            outputs['SetDepthLayerStyle']['OUTPUT'],  # depth grid layer
        ]
        layers.extend(parameters['BaseLayers'])

        # layout manager
        layoutManager = context.project().layoutManager()

        # remove layout if it already exists
        if layoutManager.layoutByName('my_map') is not None:
            layoutManager.removeLayout(layoutManager.layoutByName('my_map'))

        # create layout
        layout = QgsPrintLayout(context.project())
        layout.initializeDefaults()

        # load print layout template
        document = QDomDocument()
        with open(parameters['PrintLayout']) as f:
            document.setContent(f.read())
        c = QgsReadWriteContext()
        c.setPathResolver(context.project().pathResolver())
        c.setProjectTranslator(context.project())
        layout.readLayoutXml(document.documentElement(), document, c)
        layout.setName('my_map')

        # add print layout to layout manager
        layoutManager.addLayout(layout)

        # Export print layout as PDF
        alg_params = {
            'DISABLE_TILED': False,
            'DPI': 300,
            'FORCE_VECTOR': False,
            'GEOREFERENCE': True,
            'INCLUDE_METADATA': True,
            'LAYERS': layers,
            'LAYOUT': 'my_map',
            'SEPARATE_LAYERS': False,
            'SIMPLIFY': True,
            'TEXT_FORMAT': 0,  # Always Export Text as Paths (Recommended)
            'OUTPUT': parameters['OutputPDF']
        }
        outputs['ExportPrintLayoutAsPdf'] = processing.run('native:printlayouttopdf', alg_params, context=context,
                                                           feedback=feedback, is_child_algorithm=True)
        return results

    def name(self):
        return 'Export Map'

    def displayName(self):
        return 'Export Map'

    def group(self):
        return ''

    def groupId(self):
        return ''

    def createInstance(self):
        return ExportMap()
